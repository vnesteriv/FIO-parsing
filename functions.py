import re

from dictionary import (SERVICE_SYMBOLS,
                        ENTREPRENEUR_MANAGERS_LIST,
                        PATRONYMIC_ENDING_REPLACEMENTS,
                        NAME_REPLACEMENTS,
                        PATRONYMIC_ENDINGS,
                        NAME_MALE_ENDINGS,
                        NAME_FEMALE_ENDINGS,
                        SURNAME_ENDINGS,
                        KAZAKH_SPECIFIC)


def replace_empty_spaces(st):
    # st is lower
    st = st.replace(" - ", "-")
    st = st.replace(" – ", "-")
    return st


def clean_service_symbols(st):
    # st is lower
    cleaned_st = ""
    for s in st:
        if s not in SERVICE_SYMBOLS:
            cleaned_st += s
    return cleaned_st


def clean_entrepreneur_managers(st):
    # st is lower
    cleaned_st = st
    for word in ENTREPRENEUR_MANAGERS_LIST:
        cleaned_st = re.sub(word, "", cleaned_st)
    return cleaned_st


def clean_kazakh_specific(st):
    # st is lower
    cleaned_st = st
    for word in KAZAKH_SPECIFIC:
        cleaned_st = re.sub(word, "", cleaned_st)
    return cleaned_st


def clean_data_in_brackets(st):
    # st is lower
    # Firstly, deleting all in brackets, then deleting all when only have one opening bracket.
    st = re.sub(r"\([^)]*\)", "", st)
    return re.sub(r"\([^)]*$", "", st)


def fio_divide(fio):
    split_fio_by_space = fio.split(' ')
    len_split_fio_by_space = len(split_fio_by_space)

    try:
        if len_split_fio_by_space == 1:
            split_fio = one_elements_after_split_fio_by_space(fio)
        elif len_split_fio_by_space == 2:
            split_fio = two_elements_after_split_fio_by_space(fio)
        elif len_split_fio_by_space == 3:
            split_fio = three_elements_after_split_fio_by_space(fio)
        elif len_split_fio_by_space > 3:
            split_fio = four_elements_after_split_fio_by_space(fio)
    except Exception as e:
        print("fio_divide: could not divide: {} because of {}".format(fio, e))
        return ';;'

    return split_fio


def one_elements_after_split_fio_by_space(fio):
    split_fio = ['', '', '']
    split_fio_by_point = fio.split('.')
    split_fio_by_space = fio.split(' ')
    len_split_fio_by_point = len(split_fio_by_point)

    # Иванов
    if len_split_fio_by_point == 1:
        split_fio[0] = split_fio_by_space[0]
        split_fio[1] = ''
        split_fio[2] = ''
        return ";".join(split_fio)

    # П.С.Иванов
    elif len_split_fio_by_point == 3:
        split_fio[0] = split_fio_by_point[2]
        split_fio[1] = split_fio_by_point[0]
        split_fio[2] = split_fio_by_point[1]
        return ";".join(split_fio)

    # П.Иванов
    elif len_split_fio_by_point == 2:
        split_fio[0] = split_fio_by_point[1]
        split_fio[1] = split_fio_by_point[0]
        split_fio[2] = ""
        return ";".join(split_fio)

    # Иванов.П.Н.
    elif len_split_fio_by_point == 4 and len(split_fio_by_space) == 1:
        split_fio[0] = split_fio_by_point[0]
        split_fio[1] = split_fio_by_point[1]
        split_fio[2] = split_fio_by_point[2]
        return ";".join(split_fio)

    else:
        return ";;"


def two_elements_after_split_fio_by_space(fio):
    split_fio = ['', '', '']
    split_fio_by_point = fio.split('.')
    split_fio_by_space = fio.split(' ')

    # have 2 point
    if len(split_fio_by_point) == 3:

        # Иванов П.С.
        if len(split_fio_by_point[1]) == 1 and split_fio_by_point[2] == "":
            split_fio[0] = split_fio_by_space[0]
            split_fio[1] = split_fio_by_point[0][-1]
            split_fio[2] = split_fio_by_point[1]
            return ";".join(split_fio)

        # П.С. Иванов
        elif len(split_fio_by_point[0]) == 1 and len(split_fio_by_point[1]) == 1:
            split_fio[0] = split_fio_by_space[1]
            split_fio[1] = split_fio_by_point[0]
            split_fio[2] = split_fio_by_point[1]
            return ";".join(split_fio)

        # П. С.Иванов
        elif len(split_fio_by_point[0]) == 1 and len(split_fio_by_point[1]) == 2:
            split_fio[0] = split_fio_by_point[2]
            split_fio[1] = split_fio_by_point[0]
            split_fio[2] = split_fio_by_point[1][1]
            return ";".join(split_fio)

        else:
            return ";;"

    # have 1 point
    elif len(split_fio_by_point) == 2:

        # Иванов П.С
        if len(split_fio_by_point[1]) == 1:
            split_fio[0] = split_fio_by_space[0]
            split_fio[1] = split_fio_by_point[0][-1]
            split_fio[2] = split_fio_by_point[1]
            return ";".join(split_fio)

        # Иванов ПС.
        elif split_fio_by_point[1] == '':
            split_fio[0] = split_fio_by_space[0]
            split_fio[1] = split_fio_by_space[1][0]
            split_fio[2] = split_fio_by_space[1][1]
            return ";".join(split_fio)

        # П.С Иванов
        elif len(split_fio_by_point[0]) == 1 and len(split_fio_by_space[0]) == 3:
            split_fio[0] = split_fio_by_space[1]
            split_fio[1] = split_fio_by_point[0]
            split_fio[2] = split_fio_by_point[1][0]
            return ";".join(split_fio)

        # ПС. Иванов
        elif len(split_fio_by_point[0]) == 2:
            split_fio[0] = split_fio_by_space[1]
            split_fio[1] = split_fio_by_point[0][0]
            split_fio[2] = split_fio_by_point[0][1]
            return ";".join(split_fio)

        # П С.Иванов
        elif len(split_fio_by_space[0]) == 1 and len(split_fio_by_point[0]) == 3:
            split_fio[0] = split_fio_by_point[1]
            split_fio[1] = split_fio_by_space[0]
            split_fio[2] = split_fio_by_space[1][0]
            return ";".join(split_fio)

        # П. Иванов
        elif len(split_fio_by_point[0]) == 1 and len(split_fio_by_space[0]) == 2:
            split_fio[0] = split_fio_by_space[1]
            split_fio[1] = split_fio_by_point[0]
            split_fio[2] = ""
            return ";".join(split_fio)

        else:
            return ";;"

    # have 0 point
    elif len(split_fio_by_point) == 1:

        # Иванов ПС
        if len(split_fio_by_space[1]) == 2:
            split_fio[0] = split_fio_by_space[0]
            split_fio[1] = split_fio_by_space[1][0]
            split_fio[2] = split_fio_by_space[1][1]
            return ";".join(split_fio)

        # Иванов П
        elif len(split_fio_by_space[1]) == 1:
            split_fio[0] = split_fio_by_space[0]
            split_fio[1] = split_fio_by_space[1]
            split_fio[2] = ""
            return ";".join(split_fio)

        # ПС Иванов
        elif len(split_fio_by_space[0]) == 2:
            split_fio[0] = split_fio_by_space[1]
            split_fio[1] = split_fio_by_space[0][0]
            split_fio[2] = split_fio_by_space[0][1]
            return ";".join(split_fio)

        # П Иванов
        elif len(split_fio_by_space[0]) == 1:
            split_fio[0] = split_fio_by_space[1]
            split_fio[1] = split_fio_by_space[0]
            split_fio[2] = ""
            return ";".join(split_fio)

        # Петр Иванов
        elif len(split_fio_by_space[0]) > 1 and len(split_fio_by_space[1]) > 1:
            split_fio[0] = split_fio_by_space[0]
            split_fio[1] = split_fio_by_space[1]
            split_fio[2] = ""
            return ";".join(split_fio)

        else:
            return ";;"


def three_elements_after_split_fio_by_space(fio):
    split_fio = ['', '', '']
    split_fio_by_point = fio.split('.')
    split_fio_by_space = fio.split(' ')

    # have 2 points
    if len(split_fio_by_point) == 3:

        # Иванов П.С.
        if len(split_fio_by_point[0]) < 1 and len(split_fio_by_point[1]) == 1:
            split_fio[0] = split_fio_by_space[0]
            split_fio[1] = split_fio_by_point[0][-1]
            split_fio[2] = split_fio_by_point[1]
            return ";".join(split_fio)

        # П. С. Иванов
        elif len(split_fio_by_point[1]) == 2 and len(split_fio_by_point[0]) == 1:
            split_fio[0] = split_fio_by_space[2]
            split_fio[1] = split_fio_by_point[0]
            split_fio[2] = split_fio_by_space[1][0]
            return ";".join(split_fio)

        # Иванов П. С.
        elif len(split_fio_by_point[1]) == 2:
            split_fio[0] = split_fio_by_space[0]
            split_fio[1] = split_fio_by_point[0][-1]
            split_fio[2] = split_fio_by_space[2][0]
            return ";".join(split_fio)

        else:
            return ";;"

    # have 1 point
    elif len(split_fio_by_point) == 2:

        # П. С Иванов
        if len(split_fio_by_point[0]) == 1 and len(split_fio_by_space[1]) == 1:
            split_fio[0] = split_fio_by_space[2]
            split_fio[1] = split_fio_by_point[0]
            split_fio[2] = split_fio_by_space[1]
            return ";".join(split_fio)

        # П С. Иванов
        elif len(split_fio_by_space[0]) == 1 and len(split_fio_by_point[0]) == 3:
            split_fio[0] = split_fio_by_space[2]
            split_fio[1] = split_fio_by_space[0]
            split_fio[2] = split_fio_by_space[1][0]
            return ";".join(split_fio)

        # Иванов П С.
        elif len(split_fio_by_space[1]) == 1 and len(split_fio_by_space[2][0]) == 1:
            split_fio[0] = split_fio_by_space[0]
            split_fio[1] = split_fio_by_space[1]
            split_fio[2] = split_fio_by_space[2][0]
            return ";".join(split_fio)

        # Иванов Петр С.
        elif len(split_fio_by_space[2]) == 2:
            split_fio[0] = split_fio_by_space[0]
            split_fio[1] = split_fio_by_space[1]
            split_fio[2] = split_fio_by_space[2][0]
            return ";".join(split_fio)

        # Иванов П. Сергеевич
        elif len(split_fio_by_space[1]) == 2:
            split_fio[0] = split_fio_by_space[0]
            split_fio[1] = split_fio_by_space[1][0]
            split_fio[2] = split_fio_by_space[2]
            return ";".join(split_fio)

        else:
            return ";;"

    # have 0 point
    elif len(split_fio_by_point) == 1:

        # Иванов Петр Сергеевич
        if len(split_fio_by_space[0]) > 1 and len(split_fio_by_space[2]) > 1:
            split_fio[0] = split_fio_by_space[0]
            split_fio[1] = split_fio_by_space[1]
            split_fio[2] = split_fio_by_space[2]
            return ";".join(split_fio)

        # П С Иванов
        elif len(split_fio_by_space[0]) == 1 and len(split_fio_by_space[1]) == 1:
            split_fio[0] = split_fio_by_space[2]
            split_fio[1] = split_fio_by_space[0]
            split_fio[2] = split_fio_by_space[1]
            return ";".join(split_fio)

        # Иванов П С
        elif len(split_fio_by_space[1]) == 1 and len(split_fio_by_space[2]) == 1:
            split_fio[0] = split_fio_by_space[0]
            split_fio[1] = split_fio_by_space[1]
            split_fio[2] = split_fio_by_space[2]
            return ";".join(split_fio)

        else:
            return ";;"


def four_elements_after_split_fio_by_space(fio):
    split_fio = ['', '', '']
    split_fio_by_space = fio.split(' ')

    # Иванов – Остяков Петр Сергевич
    if len(fio.split('–')) == 2:
        split_fio[0] = (" ".join(split_fio_by_space[:3]))
        split_fio[1] = split_fio_by_space[3]
        split_fio[2] = split_fio_by_space[4]
        return ";".join(split_fio)
    else:
        fio_3 = fio.split(' ')[:3]
        return three_elements_after_split_fio_by_space(" ".join(fio_3))


def base_surname_ru(st_):
    try:
        for x in SURNAME_ENDINGS:
            st_ = re.sub(x, "", st_)
        return st_
    except Exception as e:
        print("Error: base_surname_ru: {} because of {}".format(st_, e))
        return ""


def base_name_ru(x):
    # x is name+";"+patronymic
    try:
        name, patronymic = x.split(";")
        if len(name) <= 1:
            return name

        for x in NAME_FEMALE_ENDINGS:
            if re.search(x, patronymic) is not None:
                return name
        for k, v in NAME_REPLACEMENTS.items():
            if name == k:
                return v
        for x in NAME_MALE_ENDINGS:
            name = re.sub(x, "", name)
        return name
    except Exception as e:
        print("Error: base_name_ru: {} because of {}".format(x, e))
        return ""


def base_patronymic_ru(st_):
    try:
        for k, v in PATRONYMIC_ENDING_REPLACEMENTS.items():
            if re.search(k, st_) is not None:
                return re.sub(k, v, st_)
        for x in PATRONYMIC_ENDINGS:
            st_ = re.sub(x, "", st_)
        return st_
    except Exception as e:
        print("Error: base_patronymic_ru: {} because of {}".format(st_, e))
        return ""
