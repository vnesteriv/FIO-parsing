# -*- coding: utf-8 -*-

import pandas as pd

from comparator import FIOComparator

def take_data():
    comparator = FIOComparator()
    df = pd.read_csv('fio.csv')
    # The limit for testing.
    df = df.head(10000)

    df = comparator.normalize(df)
    df.to_csv('out.csv')

if __name__ == '__main__':
    take_data()