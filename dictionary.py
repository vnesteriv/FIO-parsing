# "-" is not in this list, because fio could be "Анна-Мария"
SERVICE_SYMBOLS = [",", ":", ";", "'", '"', "?", "<", ">", "=", "+", "*", "`", "&", "_"]

# If fio contains any item from list below, it should be dropped from df. All should be lower.
DROP_LIST = [
    "управляющая компания",
    "закрытое акционерное общество",
    "w. i. out - principal",
    "государственная корпорация",
    "совет директоров",
    "ооо",
    "зао",
    "акционеры",
    "комитет",
    "генерального директора",
    "информация не представлена",
    "российская федерация",
    "государства",
    "должность",
    "федеральное агенство",
    "совет",
    "исполнительн",
    "реестр",
    "ФІЗИЧН",
]

# List of regexps and simple strings that are used to clean fio.
ENTREPRENEUR_MANAGERS_LIST = [
    "^ип",
    " ип$",
    "^врио ",
    " врио",
    "^фоп ",
    " фоп",
    "ао ",
    " ао",
    "^и. о. ",

    "[ ]?[-–]?[ ]?secretary",
    "[ ]?[-–]?[ ]?board member",
    "[ ]?[-–]?[ ]?designated limited liability partnership member",
    "[ ]?[-–]?[ ]?president",
    "[ ]?[-–]?[ ]?authorised signing officer",
    "[ ]?[-–]?[ ]?supervisory board member",
    "[ ]?[-–]?[ ]?finance manager",
    "[ ]?[-–]?[ ]?yosifova - director",
    "[ ]?[-–]?[ ]?chief executive officer",
    "[ ]?[-–]?[ ]?supervisory board president",
    "[ ]?[-–]?[ ]?supervisory board",
    "[ ]?[-–]?[ ]?principal",
    "[ ]?[-–]?[ ]?v chb",
    "[ ]?[-–]?[ ]?ceo",
    "[ ]?[-–]?[ ]?manager",
    "[ ]?[-–]?[ ]?general manager",
    "[ ]?[-–]?[ ]?principal"
    "[ ]?[-–]?[ ]?cuppen - director",

    # These should be at end, because each of this word could be in the middle of bullshit.
    "[ ]?[-–]?[ ]?managing.*$",
    "[ ]?[-–]?[ ]?chairman.*$",
    "[ ]?[-–]?[ ]?director.*$",
]

KAZAKH_SPECIFIC = [
    r"[ ]?[-–]?[ ]?оглы$",
    r"[ ]?[-–]?[ ]?кызы$",
    r"[ ]?[-–]?[ ]?оглу$",
    r"[ ]?[-–]?[ ]?улы$",
]

PATRONYMIC_ENDING_REPLACEMENTS = {
    "Яковлевич": "Яков",
    "Яковлевна": "Яков",
}

NAME_REPLACEMENTS = {
    "Михаил": "Михайл",
    "Павел": "Павл",
    "Лев": "Льв",
}

NAME_MALE_ENDINGS = ["ь$", "а$", "я$", "ай$", "ей$", "ий$", "й$"]
NAME_FEMALE_ENDINGS = ["на$", "зы$", "зи$"]
SURNAME_ENDINGS = ["ий$", "ая$", "а$"]

# To comply with the order-based. before:"еевич", then:"евич". (Алексеевич [Алекс_еевич] and no [Алексе_еевич])
PATRONYMIC_ENDINGS = [
    "иевна$",
    "иевич$",
    "ьевич$",
    "ьевна$",
    "еевич$",
    "еевна$",
    "евич$",
    "евна$",
    "ична$",
    "ович$",
    "овна$",
    "ич$",
]
