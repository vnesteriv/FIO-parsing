import list as list
import pandas as pd
import re

from dictionary import DROP_LIST
import functions as fio_funcs


class FIOComparator(object):
    @staticmethod
    def _df_fio_clean(df):
        df["fio_clean"] = df["fio"].str.lower()
        df["fio_clean"] = df["fio_clean"].apply(fio_funcs.replace_empty_spaces)
        df["fio_clean"] = df["fio_clean"].apply(fio_funcs.clean_service_symbols)
        df["fio_clean"] = df["fio_clean"].apply(fio_funcs.clean_entrepreneur_managers)
        df["fio_clean"] = df["fio_clean"].apply(fio_funcs.clean_kazakh_specific)
        df["fio_clean"] = df["fio_clean"].apply(fio_funcs.clean_data_in_brackets)
        df["fio_clean"] = df["fio_clean"].str.strip()
        df["fio_clean"] = df["fio_clean"].str.replace("  ", " ")

        return df

    @staticmethod
    def _df_drop_fio_w_bad_words(df):
        for drop_words in DROP_LIST:
            df = df.query('fio != @drop_words')

        return df

    @staticmethod
    def _df_fio_divide(df):
        l = df["fio_clean"].fillna("")
        a_s = a_n = a_p = []

        for e in l:
            try:
                t = fio_funcs.fio_divide(e)
                t = t.split(";")
                a_s = a_s + [t[0]]
                a_n = a_n + [t[1]]
                a_p = a_p + [t[2]]
            except Exception as exc:
                a_s = a_s + [""]
                a_n = a_n + [""]
                a_p = a_p + [""]
                print("ERROR: _df_fio_divide: e: {} because of {}".format(e, exc))
                pass

        add = pd.DataFrame({"surname": a_s, "name": a_n, "patronymic": a_p})
        return pd.concat([df, add], axis=1)

    @staticmethod
    def _df_base(df):
        # NOTE: probably unused
        add_1 = add_2 = add_3 = []
        l_1 = list(df["surname"])
        l_2 = list(df["name"])
        l_3 = list(df["patronymic"])

        for x in range(0, len(df)):
            add_1 = add_1 + [fio_funcs.base_surname_ru(l_1[x])]
            if len(add_2) > 1:
                add_2 = add_2 + [fio_funcs.base_name_ru("{};{}".format(l_2[x], l_3[x]))]
            else:
                add_2 = l_2[x]
            if len(add_2) > 1:
                add_3 = add_3 + [fio_funcs.base_patronymic_ru(l_3[x])]
            else:
                add_2 = l_3[x]

        return pd.concat([df, pd.DataFrame({"base_surname": add_1, "base_name": add_2, "base_patronymic": add_3})],
                         axis=1)

    @staticmethod
    def _df_fio_base(df):
        l_ru = df
        l_ru["base_surname"] = l_ru["surname"].apply(fio_funcs.base_surname_ru)
        l_ru["base_name"] = (l_ru["name"] + ";" + l_ru["patronymic"]).apply(fio_funcs.base_name_ru)
        l_ru["base_patronymic"] = l_ru["patronymic"].apply(fio_funcs.base_patronymic_ru)
        return pd.concat([l_ru], axis=0).reset_index(drop=True)

    def _normalize(self, df):
        df = df.fillna("")
        df = self._df_drop_fio_w_bad_words(df).reset_index(drop=True)

        df = self._df_fio_clean(df)

        df = self._df_fio_divide(df)
        df = self._df_fio_base(df)
        return df

    def normalize(self, df):
        return self._normalize(df)
